﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
  [SerializeField] GameObject mainMenu;
  [SerializeField] GameObject battleMenu;

  GameObject openedMenu = null;

  public void OpenMainManu()
  {
    ClosePrevMenu();
    mainMenu.SetActive( true );
    openedMenu = mainMenu;
  }

  public void OpenBattleManu()
  {
    ClosePrevMenu();
    battleMenu.SetActive( true );
    openedMenu = battleMenu;
  }

  private void ClosePrevMenu()
  {
    if ( openedMenu != null )
      openedMenu.SetActive( false );
  }
}
