﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuInput : MonoBehaviour
{
  [SerializeField] Dropdown dropdown;
  [SerializeField] Text scoreTextMesh;
  [SerializeField] Text resultTextMesh;

  public event Action<Color> OnPlayerColorChanged;
  public event Action OnSoundChanged;
  public event Action OnBattleStarted;


  public void UpdateResult( bool didPlayerWin, int killedEnemies )
  {
    scoreTextMesh.text = string.Format( "The score: {0}", killedEnemies );
    resultTextMesh.text = didPlayerWin ? "Player Won" : "Aliens Won";
  }

  public void StartBattle()
  {
    OnBattleStarted();
  }

  public void ChangeSound()
  {
    OnSoundChanged();
  }

  public void ChangeColor()
  {
    updateColor( dropdown.value );
  }

  public void updateColor( int value )
  {
    Color color;

    switch ( value )
    {
    case 0: color = Color.yellow; break;
    case 1: color = Color.blue; break;
    case 2:
    default: color = Color.yellow; break;
    }

    OnPlayerColorChanged( color );
  }
}
