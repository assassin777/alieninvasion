﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorController : MonoBehaviour
{
  [SerializeField] GameObject bulletPrefab;
  [SerializeField] float shootInterval;

  Transform battleField;

  public event Action<WarriorController> OnDied = delegate {};

  public float bulletSpeed { private get; set; }

  private float lastShootTime;


  private void Start()
  {
    battleField = GetComponentInParent<BattleController>().transform;
  }

  public void Shoot()
  {
    if ( !canShoot() )
      return;

    lastShootTime = Time.time;
    GameObject bullet = Instantiate( bulletPrefab, battleField );
    bullet.transform.position = transform.position;
    BulletController bulletController = bullet.GetComponent<BulletController>();
    bulletController.speed = bulletSpeed;
    bulletController.shooter = this;
  }

  private bool canShoot()
  {
    return lastShootTime == 0 || ( Time.time - lastShootTime ) > shootInterval;
  }

  private void OnTriggerEnter( Collider collider )
  {
    BulletController bulletController = collider.GetComponent<BulletController>();

    if ( bulletController == null )
      return;

    if ( bulletController.shooter == this )
      return;

      Die();
  }

  private void Die()
  {
    OnDied( this );
  }
}
