﻿using System;
using UnityEngine;

public class BuildingController : MonoBehaviour
{
  [SerializeField] float maxHealthInBullets;
  [SerializeField] Material material;
  [SerializeField] MeshRenderer meshRenderer;

  float curHealthInBullets;

  private void Start()
  {
    GetComponentInParent<BattleController>().OnClear += OnClear;
    OnClear();
  }

  private void OnClear()
  {
    gameObject.SetActive( true );
    meshRenderer.sharedMaterial = new Material( material );
    curHealthInBullets = maxHealthInBullets;
  }

  private void OnTriggerEnter( Collider collider )
  {
    BulletController bulletController = collider.GetComponent<BulletController>();

    if ( bulletController == null )
      return;

    --curHealthInBullets;

    if ( curHealthInBullets <= 0 )
      gameObject.SetActive( false );
    else
    {
      Color color = meshRenderer.sharedMaterial.color;
      float part = curHealthInBullets / maxHealthInBullets; 
      meshRenderer.sharedMaterial.color /= part;
    }
  }
}
