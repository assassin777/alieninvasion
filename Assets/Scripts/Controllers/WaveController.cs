﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveController : MonoBehaviour
{
  [SerializeField] GameObject enemyPrefab;
  [SerializeField] Vector3 waveStartPos;
  [SerializeField] int asidesStepBeforeForwadStep;


  [Range( 2f, 10f)]
  [SerializeField] int enemiesInRaw;
  [Range( 0.8f, 2f)]
  [SerializeField] float distantInRaw;
  [Range( 0.8f, 2f)]
  [SerializeField] float distantInColumn;

  public event Action OnWaveKilled = delegate {};

  Coroutine shootCoroutine;
  Coroutine moveCorutine;

  int enemiesInColumn;

  WarriorController[] enemies;
  List<int> shooterIndecies = new List<int>();

  public int KilledEnemies { get; private set; }


  public void Create( WaveInfo waveInfo )
  {
    KilledEnemies = 0;
    shooterIndecies.Clear();

    float enemyStartPosY = -( ( enemiesInRaw + ( enemiesInRaw + 1 ) % 2 ) * distantInRaw / 2 );

    transform.position = waveStartPos;

    enemies = new WarriorController[waveInfo.enemiesCount];

    enemiesInColumn = waveInfo.enemiesCount / enemiesInRaw;

    for ( int i = 0; i < waveInfo.enemiesCount; i++ )
    {
      GameObject enemy = Instantiate( enemyPrefab, transform );
      WarriorController warriorController = enemy.GetComponent<WarriorController>();
      enemies[i] = warriorController;
      warriorController.OnDied += OnDiedEnemy;
      warriorController.bulletSpeed = waveInfo.bulletSpeed;

      if ( i < enemiesInRaw )
        shooterIndecies.Add( i );

      float posX = ( (int)( i / enemiesInRaw ) - 1 ) * distantInColumn;
      float posY = enemyStartPosY + ( ( i % enemiesInRaw + ( ( enemiesInRaw + 1 ) % 2 ) ) * distantInRaw );
      enemy.transform.localPosition = new Vector2( posX, posY );
    }

    shootCoroutine = StartCoroutine( shootingCoroutine( waveInfo.timeBetweenShoot ) );
    moveCorutine = StartCoroutine( movingCoroutine( waveInfo.speedPerHalfSeconds ) );
  }

  public void Clear()
  {
    Array.ForEach( enemies, enemy => { if ( enemy != null ) Destroy( enemy.gameObject ); } );
  }

  private IEnumerator movingCoroutine( float speedPerSeconds )
  {
    WaitForSeconds wfs = new WaitForSeconds( 0.5f );
    int step = asidesStepBeforeForwadStep / 2;
    bool upDirection = true;

    while ( IsThereAliveEnemies )
    {
      if ( step < asidesStepBeforeForwadStep )
      {
        transform.localPosition += ( upDirection ? Vector3.up : Vector3.down ) * speedPerSeconds;
        ++step;
      } else
      {
        step = 0;
        transform.localPosition += Vector3.left * speedPerSeconds;
        upDirection = !upDirection;
      }

      yield return wfs;
    }
  }

  private IEnumerator shootingCoroutine( float timeBetweenShoot )
  {
    WaitForSeconds wfs = new WaitForSeconds( timeBetweenShoot );

    while ( IsThereAliveEnemies )
    {
      WarriorController warriorController = selectEnemyShooter();

      if ( IsAliveWarriorController( warriorController ) )
        warriorController.Shoot();

      yield return wfs;
    }
  }

  private bool IsAliveWarriorController( WarriorController warriorController )
  {
    return warriorController != null;
  }

  private WarriorController selectEnemyShooter()
  {
    return enemies[shooterIndecies[UnityEngine.Random.Range( 0, shooterIndecies.Count - 1 )]];
  }

  private void OnDiedEnemy( WarriorController enemyController )
  {
    int killedEnemyIndex = Array.IndexOf( enemies, enemyController );
    enemies[killedEnemyIndex].OnDied -= OnDiedEnemy;

    shooterIndecies.Remove( killedEnemyIndex );
    
    Destroy( enemies[killedEnemyIndex].gameObject );
    enemies[killedEnemyIndex] = null;

    ++KilledEnemies;

    if ( !IsThereAliveEnemies )
    {
      OnWaveKilled();

      if ( shootCoroutine != null )
        StopCoroutine( shootCoroutine );
    } else
    if ( killedEnemyIndex % ( enemiesInColumn - 1 ) != ( enemiesInColumn - 1 ) )
      shooterIndecies.Add( killedEnemyIndex + 1 );
  }

  private int AliveEnemies
  {
    get
    {
      return enemies.Length - KilledEnemies;
    }
  }

  private bool IsThereAliveEnemies
  {
    get
    {
      return KilledEnemies < enemies.Length;
    }
  }
}
