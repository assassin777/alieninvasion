﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
  [SerializeField] Vector3 direction;

  public float speed { private get; set; }
  public WarriorController shooter { get; set; }

  private BattleController battleController;


  private void Start()
  {
    battleController = GetComponentInParent<BattleController>();

    if ( battleController != null )
      battleController.OnClear += OnClear;
  }

  private void OnClear()
  {
    Clear();
  }

  private void Clear()
  {
    if ( battleController != null )
      battleController.OnClear -= OnClear;

    Destroy( gameObject );
  }

  private void OnTriggerEnter( Collider collider )
  {
    WarriorController warriorController = collider.GetComponent<WarriorController>();

    if ( warriorController == null || warriorController != shooter )
      Clear();
  }

  private void Update()
  {
    transform.localPosition += direction * speed * Time.deltaTime;
  }
}
