﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleController : MonoBehaviour
{
  [SerializeField] WaveController waveController;
  [SerializeField] PlayerController playerController;

  BattleInfo battle;

  public event Action OnClear = delegate {};
  public event Action<bool, int> OnEndBattle = delegate {};


  public void StartBattle()
  {
    battle = new BattleInfo();

    waveController.Create( battle.wave() );
    playerController.Create( battle.playerInfo );

    waveController.OnWaveKilled += OnWaveKilled;
    playerController.OnPlayerKilled += OnPlayerKilled;
  }

  public void EndBattle()
  {
    battle = null;

    playerController.Clear();
    waveController.Clear();
    OnClear();
  }

  private void OnWaveKilled()
  {
    if ( battle.hasEmemies() )
      waveController.Create( battle.wave() );
    else
    {
      EndBattle();
      OnEndBattle( true, waveController.KilledEnemies );
    }
  }

  private void OnPlayerKilled()
  {
    EndBattle();
    OnEndBattle( false, waveController.KilledEnemies );
  }

  private class BattleInfo
  {
    private readonly Queue<WaveInfo> wavesInfo;
    public readonly PlayerInfo playerInfo;

    public BattleInfo()
    {
      wavesInfo = new Queue<WaveInfo>();

      wavesInfo.Enqueue( new WaveInfo( 60,  0.2f, 1f,   3f   ) );
      wavesInfo.Enqueue( new WaveInfo( 80,  0.3f, 0.8f, 4f ) );
      wavesInfo.Enqueue( new WaveInfo( 100, 0.4f, 0.6f, 5f   ) );

      playerInfo = new PlayerInfo( 2f, 5f );
    }

    public bool hasEmemies()
    {
      return wavesInfo.Count > 0;
    }

    public WaveInfo wave()
    {
      return wavesInfo.Dequeue();
    }
  }
}

public struct PlayerInfo
{
  public float speed;
  public float bulletSpeed;

  public PlayerInfo( float speed, float bulletSpeed )
  {
    this.speed = speed;
    this.bulletSpeed = bulletSpeed;
  }
}

public struct WaveInfo
{
  public int enemiesCount;
  public float speedPerHalfSeconds;
  public float timeBetweenShoot;
  public float bulletSpeed;

  public WaveInfo( int enemiesCount, float speedPerHalfSeconds, float timeBetweenShoot, float bulletSpeed )
  {
    this.enemiesCount = enemiesCount;
    this.speedPerHalfSeconds = speedPerHalfSeconds;
    this.timeBetweenShoot = timeBetweenShoot;
    this.bulletSpeed = bulletSpeed;
  }
}
