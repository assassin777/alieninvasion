﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
  [SerializeField] GameObject playerPrefab;

  public Color playerColor { private get; set; }

  public event Action OnPlayerKilled = delegate {};

  bool moveRight;
  bool moveLeft;

  WarriorController warriorController;

  float speed;


  public void Create( PlayerInfo playerInfo )
  {
    warriorController = Instantiate( playerPrefab, transform ).GetComponent<WarriorController>();

    warriorController.GetComponent<MeshRenderer>().sharedMaterial.SetColor( "_Color", playerColor );

    speed = playerInfo.speed;
    warriorController.bulletSpeed = playerInfo.bulletSpeed;
    warriorController.OnDied += OnDied;

  }

  private void OnDied( WarriorController warriorController )
  {
    Clear();
    OnPlayerKilled();
  }

  public void Clear()
  {
    if ( isDead() )
      return;

    Destroy( warriorController.gameObject );
  }

  public void shoot()
  {
    if ( isDead() )
      return;

    warriorController.Shoot();
  }

  public void MoveLeftBtnDown()
  {
    if ( moveRight )
      return;

    moveLeft = true;
  }

  public void MoveLeftBtnUp()
  {
    if ( moveRight )
      return;

    moveLeft = false;
  }

  public void MoveRightBtnDown()
  {
    if ( moveLeft )
      return;

    moveRight = true;
  }

  public void MoveRightBtnUp()
  {
    if ( moveLeft )
      return;

    moveRight = false;
  }

  private void Update()
  {
    if ( isDead() )
      return;

    if ( moveLeft )
      warriorController.transform.localPosition += speed * Vector3.up * Time.deltaTime;
    else
    if ( moveRight )
      warriorController.transform.localPosition += speed * Vector3.down * Time.deltaTime;
  }

  private bool isDead()
  {
    return warriorController == null;
  }
}
