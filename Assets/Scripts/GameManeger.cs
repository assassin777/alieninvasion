﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManeger : MonoBehaviour
{
  [SerializeField] UIManager uiManager;
  [SerializeField] MainMenuInput mainMenuInput;
  [SerializeField] AudioSource audioSource;
  [SerializeField] BattleController battleController;
  [SerializeField] PlayerController playerController;

  private int allEnemiesKilled;

  public void Exit()
  {
    battleController.EndBattle();
    uiManager.OpenMainManu();
  }

	private void Start ()
  {
    BeginGame();

    mainMenuInput.OnPlayerColorChanged += OnPlayerColorChanged;
    mainMenuInput.OnBattleStarted += OnBattleStarted;
    mainMenuInput.OnSoundChanged += OnSoundChanged;
    battleController.OnEndBattle += OnEndBattle;
	}

  private void BeginGame()
  {
    uiManager.OpenMainManu();
  }

  private void OnEndBattle( bool didPlayerWin, int killedEnemies )
  {
    uiManager.OpenMainManu();
    allEnemiesKilled += killedEnemies;
    mainMenuInput.UpdateResult( didPlayerWin, allEnemiesKilled );
  }

  private void OnPlayerColorChanged( Color color )
  {
    playerController.playerColor = color;
  }

  private void OnBattleStarted()
  {
    uiManager.OpenBattleManu();
    battleController.StartBattle();
  }

  private void OnSoundChanged()
  {
    if ( audioSource.isPlaying )
      audioSource.Stop();
    else
      audioSource.Play();
  }
}
